const BASE_API = `https://jsonplaceholder.typicode.com/`;
const services = {
    allPosts: `posts/`,
    comments: `comments/`

}

const getAllPosts = () => {
    return new Promise((resolve, reject)=>{
        try {
            let url = BASE_API + services.allPosts
            fetch(url)
            .then(res=>res.json())
            .then(res=>resolve(res))
        } catch(err) {
            reject(err)
        }
    })
}

const getPost = (id) => {
    return new Promise((resolve, reject)=>{
        try {
            let url = BASE_API + services.allPosts + id
            fetch(url)
            .then(res=>res.json())
            .then(res=>resolve(res))
        } catch(err) {
            reject(err)
        }
    })
}

const getComments = (id) => {
    return new Promise((resolve, reject)=>{
        try {
            let url = BASE_API + services.allPosts + id + '/' + services.comments
            fetch(url)
            .then(res=>res.json())
            .then(res=>resolve(res))
        } catch(err) {
            reject(err)
        }
    })
}

module.exports = {
    getAllPosts,
    getPost,
    getComments
}