import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './App.css';
import Posts from './components/PostDashboard';
import Post from './components/PostData';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' Component={Posts} />
          <Route path='/:id' Component={Post} />
        </Routes>
      </Router>
      {/* <Posts /> */}
    </div>
  );
}

export default App;
