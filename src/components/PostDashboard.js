import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';


import { 
    getAllPosts,
} from '../services/services';

function Posts() {
    const [posts, setPosts] = useState([]);


    useEffect(()=> {
        getAllPosts()
        .then(postData=>setPosts(postData))
    }, [])

    return (
        <div className="post_dashboard">
            {
                posts.map(post=>(
                    <Card sx={{ maxWidth: 345, margin: "10px", background: 'linear-gradient(#c1fff7, #ebfbff)', minHeight: "270px" }}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                {post.title}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {post.body}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={'/'+post.id} key={post.id}>
                                <Button size="small">More</Button>
                            </Link>
                        </CardActions>
                    </Card>
                ))
            }
        </div>
    )
}

export default Posts