import { useState, useEffect } from "react";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import { getComments } from "../services/services";

function Comments({ postId }) {
    const [commentList, setCommentList] = useState([]);
    const [viewCommand, setViewCommand] = useState(false)

    const handleCommands = () => {
        setViewCommand(!viewCommand)
    }

    useEffect(() => {
        if (postId) {
            getComments(postId)
                .then(res => setCommentList(res))
        }
    }, [postId, viewCommand])
    return (
        <div>
            <CardActions>
                <Button onClick={handleCommands} endIcon={viewCommand?<ExpandLessIcon/>:<ExpandMoreIcon/>} size="small">Comments</Button>
            </CardActions>
            {
                viewCommand?commentList.map(comment => (
                    <Card sx={{ maxWidth: '80%', margin: "0 auto 10px auto", boxShadow: "none" }}>
                        <CardContent>
                            <Typography align="justify" gutterBottom component="div">
                                {comment.body}
                            </Typography>
                            <Typography align="right" variant="subtitle2" color="text.secondary">
                                {comment.name}
                            </Typography>
                            <Typography align="right" variant="subtitle2" color="text.secondary">
                                {comment.email}
                            </Typography>
                        </CardContent>
                    </Card>
                    // <div key={comment.id} style={{padding: "10px", border: "1px solid #000", marginBottom: "10px", display: "flex", flexDirection: "column"}}>
                    //     <div>{comment.body}</div>
                    //     <div>{comment.name}</div>
                    //     <div>{comment.email}</div>
                    // </div>
                )) : <></>
            }
        </div>
    )
}

export default Comments