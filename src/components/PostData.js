import { useEffect, useState } from "react";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';


import { 
    getPost
} from '../services/services';
import Comments from './Comments'

function Post() {
    let postId = window.location.href.split('/')[window.location.href.split('/').length-1]
    const [postData, setPostData] = useState({})

    useEffect(()=>{
        if (postId) {
            getPost(postId)
            .then(postData=>setPostData(postData))
        }
    }, [postId])
    console.log(postData)
    return (
        <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
            <Card sx={{ maxWidth: '70%', margin: "10px", background: '#c1fff7' }}>
                {
                    Object.keys(postData).length>0?(
                        <>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {postData.title}
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    {postData.body}
                                </Typography>
                            </CardContent>
                            <Comments postId={postId}/>
                        </>
                    ) : (
                        <CircularProgress/>

                    )
                }
            </Card>
        </div>
    )
}

export default Post